# Framework Structure

 
````
***
├───driver [This is driver folder]
│       chromedriver.exe [Chrome driver version](https://googlechromelabs.github.io/chrome-for-testing/)
│       geckodriver.exe [Firefox driver version](https://github.com/mozilla/geckodriver/releases)
│       msedgedriver.exe  [Edge driver version](https://developer.microsoft.com/en-us/microsoft-edge/tools/webdriver/)  
│       
└───src
    │   config.properties [Config env before testing here URL,browser,...]
    ├───main
    │   └───java
    │       ├───base [Base setup folder]
    │       │       BaseSetup.java [Use to setup before, after run and Environment config]
    │       │       Browser.java [Use to define the browser base on config which is read in the BaseSetUp file ^]
    │       │       
    │       ├───common
    │       │       ContextFunction.java [read ${__Context()} to replace or execute function ]
    │       │       CucumberContext.java [This is Class to store the CucumberContext for easy to reuse in other file]
    │       │       DataField.java [Code to interact with csv/excel file]
    │       │       EnvironmentSetup.java [This is Class to store Environment setup from BaseSetup file]
    │       │       ExecuteTestData.java [Code replace value in excel file with value form database to verify data]
    │       │       PageObject.java [Basic step like click, sendkey,...]
    │       │       ScenarioLogger.java [Use to log scenario when run/ debug]
    │       │       TestCaseSQL.java [Read syntax from SQL file and Execute it]
    │       │       VerifyDB.java [Verify Database after run with expected result]
    │       │       VerifyUI.java [Verify UI after run with expected result]
    │       │       
    │       ├───org
    │       │   └───example
    │       │           Main.java [It use for test algorithm correctness when build framework]
    │       │           
    │       ├───pageObject [this is Page object test which will contain xpath, function]
    │       │       CommonPageObject.java
    │       │       OtherPage.java
    │       │       
    │       └───utils
    │               CommonUtils.java [Common java function like random number, date, replace,..]
    │               DBUtils.java [Utilities to connect to database]
    │               ExcelUtils.java [Utilities to connect excel/csv file]
    │               FileSystem.java [Use to create/read directory, zip file when report ***Not Done***]
    │               Log.java [Easy tracking testcase when run ***Not Done***]
    │               
    └───test [Test framework folder]
        ├───java
        │   ├───runner
        │   │       Runner.java
        │   │       
        │   └───stepdefinitions [Step define to use Gherkin language]
        │           CommonStepDefinitions.java
        │           OtherDefinitions.java
        │           
        └───resources
            └───features [Cucumber feature file and SQL/Excel]
                └───demo
                    │   demo_test.feature
                    │   
                    └───open_web
                            open_web.sql
                            open_web.xlsx
                            
***
````
