package stepdefinitions;

import base.BaseSetup;
import common.CucumberContext;
import common.DataField;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import pageObject.OtherPage;
import utils.ExcelUtils;

public class OtherDefinitions {

    public WebDriver driver;
    public Scenario scenario;
    ExcelUtils dataPool;
    DataField dataField;
    CucumberContext context;
    OtherPage otherPage;

    public OtherDefinitions() throws Exception {
        driver = BaseSetup.driver;
        this.scenario = BaseSetup.scenario;
        this.context = BaseSetup.context;
        this.dataField = context.getDataField();
        this.otherPage = new OtherPage(driver, dataField);
    }



    @When("Verify the UI is matching with database")
    public void enter_the_Username_and_Password() throws Throwable {
        otherPage.printExcelData();
    }

    @Then("User navigates to form")
    public void Reset_the_credential() throws Throwable {
        otherPage.navigateToFrom();
    }
}
