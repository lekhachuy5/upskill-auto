package org.example;


import org.mvel2.MVEL;

import java.io.IOException;
import java.util.*;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {


    public static void main(String[] args) throws IOException, InterruptedException {

        List<Map<String, String>> list = new ArrayList<>();
        Random rand = new Random();
        String[] items = {"item1", "item2", "item3", "item4", "item5"};
        for (int i = 0; i < 5; i++) {
            Map<String, String> map = new HashMap<>();
            map.put("column", items[i]);
            map.put("data", "1");
            list.add(map);
        }
        Map<String,String> object = new HashMap<>();
        object.put("column","data");
        object.put("data","1");
        object.put("condition","equals");
        String expression = "";
        if(object.get("condition").equals("equals")){
            expression = String.format(" item.get('%s') == %s", object.get("column"), object.get("data"));
        }else{
            expression = String.format("\"%s\" %s \"%s\"", object.get("column"),object.get("condition"), 1);
        }
        System.out.println(expression);
        Object  finalExpression = MVEL.compileExpression(expression);
        System.out.println(finalExpression);
        List<Map<String, String>> filteredList = list.stream()
                .filter(item -> (Boolean) MVEL.executeExpression(finalExpression,item))
                .toList();
        System.out.println(filteredList);
    }
}