package common;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExecuteTestData {
    private static final Logger logger = LoggerFactory.getLogger(ExecuteTestData.class);
    public ContextFunction contextFunction = new ContextFunction();

    public ExecuteTestData() {
    }

    public String getReplacementValue(Object testCaseValue, Map<String, String> sqlStatements, CucumberContext context) throws Exception {
        String newValue = (String) testCaseValue;
        if (testCaseValue != null && testCaseValue.toString().startsWith("${__")) {
            String testCaseValueStr = testCaseValue.toString();
            if (testCaseValueStr.substring(2, testCaseValueStr.indexOf(40)).equals("__Context")) {
                contextFunction.setContext(context.getTestDataContext());
                String paramsStr = testCaseValueStr.substring(testCaseValueStr.indexOf(40) + 1, testCaseValueStr.lastIndexOf(41));
                String[] paramsArr = paramsStr.split(",");
                Collection<String> paramsColl = new ArrayList<>(paramsArr.length);
                paramsColl.addAll(Arrays.asList(paramsArr).subList(0, paramsArr.length));
                contextFunction.setParameters(paramsColl);
                newValue = contextFunction.execute();
            }
        }
        return StringUtils.defaultString(newValue);
    }
}
