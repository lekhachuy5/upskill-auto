package common;

import java.io.InvalidClassException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ContextFunction {
    private static final Logger logger = LoggerFactory.getLogger(ContextFunction.class);
    private static Pattern contextRegex = Pattern.compile("\\$\\{\\_\\_Context\\(([a-z|A-Z|0-9| _\\.|\\[|\\]]*)\\)\\}");
    private String contextKey;
    private Map<String, Object> context;

    public ContextFunction() {
    }

    public static String replaceContextSyntax(String content, Map<String, Object> context, int index) {
        content = StringUtils.replace(content, "[i]", "[" + String.valueOf(index) + "]");
        Matcher matcher = contextRegex.matcher(content);
        StringBuilder newContent = new StringBuilder(content.length());

        int lastMatchEnd;
        for(lastMatchEnd = 0; matcher.find(); lastMatchEnd = matcher.end()) {
            content = StringUtils.replace(content, "[i]", "[" + String.valueOf(index) + "]");
            newContent.append(content.substring(lastMatchEnd, matcher.start()));
            ContextFunction contextFunction = new ContextFunction();
            contextFunction.setContext(context);
            Collection<String> params = new ArrayList();
            params.add((matcher.group(1)));

            try {
                contextFunction.setParameters(params);
                newContent.append(contextFunction.execute());
            } catch (InvalidClassException var9) {
                logger.error("Unable to replace Context function {}", matcher.group(0), var9);
                throw new RuntimeException("Unable to replace Context function " + matcher.group(0), var9);
            } catch (Exception var10) {
                logger.error("Unable to replace Context function {}", matcher.group(0), var10);
                return null;
            }
        }

        if (lastMatchEnd > 0) {
            newContent.append(content.substring(lastMatchEnd));
            content = newContent.toString();
        }

        if (contextRegex.matcher(content).find()) {
            content = replaceContextSyntax(content, context, index);
        } else if (content.equals("null")) {
            return "";
        }

        return content.trim();
    }

    public static String replaceContextSyntax(String content, Map<String, Object> context) {
        Matcher matcher = contextRegex.matcher(content);
        StringBuilder newContent = new StringBuilder(content.length());

        int lastMatchEnd;
        for(lastMatchEnd = 0; matcher.find(); lastMatchEnd = matcher.end()) {
            newContent.append(content.substring(lastMatchEnd, matcher.start()));
            ContextFunction contextFunction = new ContextFunction();
            contextFunction.setContext(context);
            Collection<String> params = new ArrayList<>();
            params.add((matcher.group(1)));

            try {
                contextFunction.setParameters(params);
                newContent.append(contextFunction.execute());
            } catch (InvalidClassException var8) {
                logger.error("Unable to replace Context function {}", matcher.group(0), var8);
                throw new RuntimeException("Unable to replace Context function " + matcher.group(0), var8);
            } catch (Exception var9) {
                logger.error("Unable to replace Context function {}", matcher.group(0), var9);
                return null;
            }
        }

        if (lastMatchEnd > 0) {
            newContent.append(content.substring(lastMatchEnd));
            content = newContent.toString();
        }

        if (content == null) {
            return StringUtils.defaultString(content);
        } else {
            if (contextRegex.matcher(content).find()) {
                content = replaceContextSyntax(content, context);
            } else if (content.equals("null")) {
                return "";
            }

            return content.trim();
        }
    }

    public String execute() {
        try {
            return this.context instanceof Map && this.context.get(this.contextKey.substring(0, this.contextKey.indexOf("."))) == null ? "" : BeanUtils.getProperty(this.context, this.contextKey);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException var4) {
            logger.error("Unable to get property for contextKey {}", this.contextKey, var4);
            throw new RuntimeException("Unable to get property for contextKey " + this.contextKey, var4);
        }
    }

    public void setParameters(Collection<String> parameters) throws Exception {
        Object[] values = parameters.toArray();
        this.contextKey = values[0].toString();
    }

    public void setContext(Map<String, Object> context) {
        this.context = context;
    }

}
