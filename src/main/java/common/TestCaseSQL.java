package common;

import utils.DBUtils;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;

public class TestCaseSQL {
    private static Logger logger = LoggerFactory.getLogger(TestCaseSQL.class);

    public TestCaseSQL() {
    }

    public static void beforeTestCase(CucumberContext context) throws Exception {
        System.out.println("beforeTestRun");
        runSqlAndPopulateContext("beforeTestRun", "beforeTestRunResult", context);
        System.out.println("beforeTestRun_");
        runSqlAndPopulateContext("beforeTestRun_" + context.getTestCaseId(), "beforeTestRunResult", context);
    }

    public static void afterTestCase(CucumberContext context) throws Exception {
        System.out.println("afterTestRun");
        runSqlAndPopulateContext("afterTestRun", "afterTestRunResult", context);
        System.out.println("afterTestRun_");
        runSqlAndPopulateContext("afterTestRun_" + context.getTestCaseId(), "afterTestRunResult", context);
    }

    public static void afterTestCase(CucumberContext context, String queryNo) throws Exception {
        runSqlAndPopulateContext("afterTestRun" + queryNo, "afterTestRunResult" + queryNo, context);
        runSqlAndPopulateContext("afterTestRun" + queryNo + "_" + context.getTestCaseId(), "afterTestRunResult" + queryNo, context);
    }

    public static void apiGenerateTest(CucumberContext context) throws Exception {
        runSqlAndPopulateContext("apiGenerateRun", "apiGenerateRunResult", context);
        runSqlAndPopulateContext("apiGenerateRun" + "_" + context.getTestCaseId(), "apiGenerateRunResult" , context);
    }
    public static void runSqlAndPopulateContext(String key, String resultKey, CucumberContext context) throws Exception {
        Map<String, String> sqlStatements = null;
        sqlStatements = loadSqlStatements((CucumberContext)context, (String)null);
        if (sqlStatements != null && sqlStatements.size() > 0) {
            context.setSqlStatements(sqlStatements);
            if (sqlStatements.containsKey(key)) {
                String query = ContextFunction.replaceContextSyntax((String)sqlStatements.get(key)
                        , context.getTestDataContext());
                System.out.println(query);
                context.addFinalQuery(key, query);
                Object result = DBUtils.getListOfRecords(query);
                System.out.println("\n Result sets ::" + result);
                context.getTestDataContext().put(resultKey, result);
            }
        }

    }

    public static Map<String, String> loadSqlStatements(CucumberContext context, String filePrefix) throws Exception {
        String level = "feature";
        String sqlStatementFileName = getSqlStatementFileName(context, filePrefix, level);
        Map<String, String> sqlStatements = loadSqlStatements(sqlStatementFileName, context);
        logger.debug("Loaded {} SQL statements from file {}", sqlStatements.size(), sqlStatementFileName);
        level = "scenario";
        sqlStatementFileName = getSqlStatementFileName(context, filePrefix, level);
        Map<String, String> scenarioSqlStmts = loadSqlStatements(sqlStatementFileName, context);
        logger.debug("Loaded {} SQL statements from file {}", scenarioSqlStmts.size(), sqlStatementFileName);
        sqlStatements.putAll(scenarioSqlStmts);
        return sqlStatements;
    }

    public static String getSqlStatementFileName(CucumberContext context, String filePrefix, String level) {
        String fileName;
        if (filePrefix == null) {
            if (level.equals("feature")) {
                fileName = getFeatureDirectory(context);
                fileName = fileName + "/" + fileName.split("/")[2] + ".sql";
            } else {
                fileName = getFilePrefix(context);
                fileName = fileName + ".sql";
            }
        } else {
            fileName = getFeatureDirectory(context) + "/" + filePrefix + ".sql";
        }

        return fileName;
    }

    public static String getFeatureDirectory(CucumberContext context) {
        return "/features/" + context.getScenario().getId().split(";")[0].replace("-", "_");
    }

    public static String getFilePrefix(CucumberContext context) {
        String filePath = StringUtils.substringAfter(context.getScenario().getUri().toString(), "features/");
        filePath = StringUtils.substringBeforeLast(filePath, "/");
        filePath = filePath + "/" + StringUtils.substringBefore(context.getScenario().getName(), "-").trim().replace(" ", "_");
        filePath = filePath + "/" + StringUtils.substringBefore(context.getScenario().getName(), "-").trim().replace(" ", "_");
        return "/features/" + StringUtils.lowerCase(filePath);
    }

    public static String getTestCaseDetails(CucumberContext context) {
        int lineNum = context.getScenario().getLine();
        String testcaseNameId = getFilePrefix(context);
        String[] scenarioName = testcaseNameId.split("/");
        int count = countOccurences(testcaseNameId, '/', 0);
        testcaseNameId = scenarioName[count - 2].toUpperCase() + "_" + scenarioName[count - 1] + "_" + lineNum;
        return testcaseNameId;
    }

    public static int countOccurences(String someString, char searchedChar, int index) {
        if (index >= someString.length()) {
            return 0;
        } else {
            int count = someString.charAt(index) == searchedChar ? 1 : 0;
            return count + countOccurences(someString, searchedChar, index + 1);
        }
    }

    public static Map<String, String> loadSqlStatements(String fileName, CucumberContext context) throws Exception {
        HashMap sqlStatements = new HashMap();

        try {
            ClassPathResource cpr = new ClassPathResource(fileName);
            if (cpr.exists()) {
                File sqlFile = cpr.getFile();
                context.getScenario().attach(IOUtils.toByteArray(new FileInputStream(sqlFile)), "application/sql",fileName);
                String sqlStr = IOUtils.toString(new FileReader(sqlFile));
                String[] sqlArr = sqlStr.split("--");
                String[] currSqlArr = sqlArr;

                for(int i = 0; i < sqlArr.length; ++i) {
                    String statement = currSqlArr[i].trim();
                    if (statement.length() > 0) {
                        String key = statement.substring(0, statement.indexOf(10)).trim();
                        sqlStatements.put(key, statement.substring(statement.indexOf(10) + 1));
                    }
                }

                logger.debug("Loaded {} SQL statements from file {}", sqlStatements.size(), fileName);
            }

            return sqlStatements;
        } catch (IOException var12) {
            throw var12;
        }
    }
}