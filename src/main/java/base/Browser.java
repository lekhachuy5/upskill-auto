package base;

import io.github.bonigarcia.wdm.WebDriverManager;

import java.sql.SQLOutput;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.SessionNotCreatedException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

public class Browser {
    private static DesiredCapabilities dr = null;
    private static String FIREFOX_BROWSER_NAME = "firefox";
    private static String CHROME_BROWSER_NAME = "chrome";
    private static String EDGE_BROWSER_NAME = "edge";
    private String CurrentDirectory;
    private static String FF_DRIVER_PATH = "../../../driver/geckodriver.exe";
    private static String CH_DRIVER_PATH = "../../../driver/chromedriver.exe";
    private static String ED_DRIVER_PATH = "../../../driver/msedgedriver.exe";

    public Browser() {
    }

    public WebDriver setLocalDriver(String browserName) throws Exception {
        this.CurrentDirectory = System.getProperty("user.dir");
        if (browserName.equalsIgnoreCase(FIREFOX_BROWSER_NAME)) {
            WebDriverManager.firefoxdriver().setup();
            FirefoxBinary firefoxBinary = new FirefoxBinary();
            if (BaseSetup.environmentSetup.isHeadless()) {
                firefoxBinary.addCommandLineOptions(new String[]{"--headless"});
            }
            FirefoxOptions firefoxOptions = new FirefoxOptions();
            firefoxOptions.setBinary(firefoxBinary);
            if (BaseSetup.environmentSetup.isHeadless()) {
                try {
                    WebDriver ff = new FirefoxDriver(firefoxOptions);
                    ff.manage().window().setSize(new Dimension(1920, 1080));
                    return ff;
                } catch (SessionNotCreatedException snce) {
                    System.setProperty("webdriver.gecko.driver", FF_DRIVER_PATH);
                    WebDriver ff = new FirefoxDriver(firefoxOptions);
                    ff.manage().window().setSize(new Dimension(1920, 1080));
                    return ff;
                } catch (NotFoundException notFoundException) {
                    System.out.println("Can't found driver or driver not support");
                    return null;
                }
            } else {
                try {
                    return new FirefoxDriver(firefoxOptions);
                } catch (SessionNotCreatedException snce) {
                    System.setProperty("webdriver.gecko.driver", FF_DRIVER_PATH);
                    return new FirefoxDriver(firefoxOptions);
                } catch (NotFoundException notFoundException) {
                    System.out.println("Can't found driver or driver not support");
                    return null;
                }
            }
        } else if (browserName.equalsIgnoreCase(EDGE_BROWSER_NAME)) {
            WebDriverManager.edgedriver().setup();
            EdgeOptions edgeOptions = new EdgeOptions();
            if (BaseSetup.environmentSetup.isHeadless()) {
                edgeOptions.addArguments(new String[]{"headless"});
            }
            if (BaseSetup.environmentSetup.isHeadless()) {
                try {
                    WebDriver ed = new EdgeDriver(edgeOptions);
                    ed.manage().window().setSize(new Dimension(1920, 1080));
                    return ed;
                } catch (SessionNotCreatedException snce) {
                    System.setProperty("webdriver.edge.driver", ED_DRIVER_PATH);
                    WebDriver ed = new EdgeDriver(edgeOptions);
                    ed.manage().window().setSize(new Dimension(1920, 1080));
                    return ed;
                } catch (NotFoundException notFoundException) {
                    System.out.println("Can't found driver or driver not support");
                    return null;
                }
            } else {
                try{
                return new EdgeDriver(edgeOptions);
            } catch (SessionNotCreatedException snce) {
                System.setProperty("webdriver.edge.driver", ED_DRIVER_PATH);
                    return new EdgeDriver(edgeOptions);
            } catch (NotFoundException notFoundException) {
                System.out.println("Can't found driver or driver not support");
                return null;
            }
            }
        } else {
            if (System.getProperty("webdriver.chrome.driver") == null) {
                WebDriverManager.chromedriver().setup();
            }

            WebDriverManager.chromedriver().setup();
            ChromeOptions chromeOptions = new ChromeOptions();
            chromeOptions.addArguments(new String[]{"--disable-site-isolation-trials"});
            if (BaseSetup.environmentSetup.isHeadless()) {
                chromeOptions.addArguments(new String[]{"--headless", "start-maximized", "enable-automation", "--no-sandbox", "--disable-dev-shm-usage", "--disable-browser-side-navigation", "--disable-gpu"});
            }

            if (BaseSetup.environmentSetup.getDownloadPath() != null && !BaseSetup.environmentSetup.getDownloadPath().isEmpty()) {
                Map<String, Object> prefs = new HashMap();
                prefs.put("download.default_directory", BaseSetup.environmentSetup.getDownloadPath());
                chromeOptions.setExperimentalOption("prefs", prefs);
                System.out.println(prefs);
            }
            if (BaseSetup.environmentSetup.isHeadless()) {
                try {
                    WebDriver ch = new ChromeDriver(chromeOptions);
                    ch.manage().window().setSize(new Dimension(1920, 1080));
                    return ch;
                } catch (SessionNotCreatedException snce) {
                    System.setProperty("webdriver.chrome.driver", CH_DRIVER_PATH);
                    WebDriver ch = new ChromeDriver(chromeOptions);
                    ch.manage().window().setSize(new Dimension(1920, 1080));
                    return ch;
                } catch (NotFoundException notFoundException) {
                    System.out.println("Can't found driver or driver not support");
                    return null;
                }
            } else {
                try {
                    return new ChromeDriver(chromeOptions);
                } catch (SessionNotCreatedException snce) {
                    System.setProperty("webdriver.chrome.driver", CH_DRIVER_PATH);
                    return new ChromeDriver(chromeOptions);
                } catch (NotFoundException notFoundException) {
                    System.out.println("Can't found driver or driver not support");
                    return null;
                }
            }
        }
    }
}
