package pageObject;

import base.BaseSetup;
import common.DataField;
import common.VerifyUI;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.List;

public class OtherPage extends CommonPageObject{
    public static WebDriver edriver;
    public static DataField datafield;

    @FindBy(how = How.XPATH, using = "//b[text() = ' Đăng ký thủ tục']/..")
    public WebElement formText;
    @FindBy(how = How.XPATH, using = "//div[@class='panel-title'][1]//h4[1]")
    public WebElement category;
    @FindBy(how = How.XPATH, using = "//div[@class='panel-title'][1]//h4")
    public List<WebElement> categoryList;
    @FindBy(how = How.XPATH, using = "//div[@id='notificationModal']//div[@class='modal-content']")
    public WebElement warningModal;
    @FindBy(how = How.XPATH, using = "//div[@id='notificationModal']//div[@class='modal-footer']//a")
    public WebElement closeModal;
    public OtherPage(WebDriver driver, DataField dataField) throws Exception {
        super(driver);
        edriver = driver;
        this.datafield = dataField;
    }

    public void printExcelData() throws Exception {
        for (WebElement e:categoryList
             ) {
            e.click();
        }

        BaseSetup.context.setVerifyPageObj(this);
        VerifyUI verifyUI = new VerifyUI(BaseSetup.context);
        verifyUI.setRowCount(Integer.parseInt(datafield.getData("row_count")));
        verifyUI.setElementsToVerify(new String[]{"VerifyUI.category"});
        verifyUI.compareUIElements();
    }

    public void navigateToFrom() throws Exception {
        waitUntilElementIsVisible(formText);
        click(formText);
        waitUntilElementIsVisible(category);
        waitUntilElementIsVisible(warningModal);
        click(closeModal);
    }
}
