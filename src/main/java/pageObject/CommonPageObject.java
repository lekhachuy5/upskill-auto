package pageObject;

import base.BaseSetup;
import common.DataField;
import common.PageObject;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class CommonPageObject extends PageObject {
    public static WebDriver edriver;
    public static DataField datafield;

    public static String url = BaseSetup.properties.getProperty("data.url").endsWith("\\u002F") ?
            BaseSetup.properties.getProperty("data.url").replace("\\u002F", "") :
            BaseSetup.properties.getProperty("data.url");

    @FindBy(how = How.XPATH, using = "//div[@class='sc-124al1g-2 dwOYCh']//button[@class='sc-124al1g-0 jCsgpZ']")
    public WebElement button;
    public CommonPageObject(WebDriver driver) throws Exception {
        edriver = driver;
        PageFactory.initElements(edriver, this);
        setDriver(edriver);
    }

    public CommonPageObject(WebDriver driver, DataField dataField) throws Exception {
        this(driver);
        this.datafield = dataField;
    }


    public void closeWeb() throws Exception {
        edriver.close();
    }

    public void sendKeys(WebElement Element) throws Exception {
        waitUntilElementIsVisible(Element);
        sendKeys(Element);
    }

    public void sendKeys(String xpath) throws Exception {
        waitUntilElementIsVisible(xpath);
        sendKeys(xpath);
    }


//    public String getTokenForApi() {
//        RequestSpecification request = RestAssured.given()
//                .contentType("application/x-www-form-urlencoded; charset=utf-8")
//                .header("Authorization", "")
//                .formParam("grant_type", "password")
//                .formParam("username", BaseSetup.properties.getProperty("data.username"))
//                .formParam("password", BaseSetup.properties.getProperty("data.password"))
//                .formParam("scope", "email roles");
//        String username = BaseSetup.properties.getProperty("data.username");
//        String password =BaseSetup.properties.getProperty("data.password");
//        request.baseUri(url);
//        Response response = request.post("");
//        String resString = response.jsonPath().getString("access_token");
//        return resString;
//    }

    public List<Map<String,String>> returnContextTable(String contextName, int tableIndex){
        Object totalRecords = (((ArrayList)(BaseSetup.context.getTestDataContext().get(contextName))).get(tableIndex));
        return (List<Map<String, String>>) totalRecords;
    }


    public List<Map<String,String>> returnContextTable(String contextName, int tableIndex,String filterLabel, String filterValue){
        Object totalRecords = (((ArrayList)(BaseSetup.context.getTestDataContext().get(contextName))).get(tableIndex));
        List<Map<String, String>> results = returnContextTable("onAfterTestCaseQueryResult", tableIndex).stream()
                .filter(x -> x.get(filterLabel).equals(filterValue)
                ).collect(Collectors.toList());
        Assert.assertEquals(results.size(), 0);
        return (List<Map<String, String>>) totalRecords;
    }


//    public void runAPI(String method, String api, JSONObject payload, int status) {
////        String token = getTokenForApi();
//        RequestSpecification request1 = RestAssured.given()
//                .header("Authorization", "Bearer " + token)
//                .contentType("application/json").baseUri(url)
//                .body(payload.toString());
//        if (method.equals("post")) {
//            Response response1 = request1.post(api);
//            System.out.println(response1.asString());
//            /*
//             * To perform validation on response like status code or value, we need to get
//             * ValidatableResponse type of response using then() method of Response
//             * interface. ValidatableResponse is also an interface.
//             */
//            ValidatableResponse valRes = response1.then();
//            valRes.statusCode(status);
//        } else {
//            Response response1 = request1.patch(api);
//            System.out.println(response1.asString());
//            ValidatableResponse valRes = response1.then();
//            valRes.statusCode(status);
//        }
//    }

    public JSONObject setJsonObject(String context, int tableIndex, int row) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        Set<String> key = returnContextTable(context, tableIndex).get(row).keySet();
        for (String i : key
        ) {
            try {
                if (i.startsWith("bool_")) {
                    jsonObject.put(i.replace("bool_", ""), Boolean.parseBoolean(returnContextTable(context, tableIndex).get(row).get(i)));
                } else if (i.startsWith("int_")) {
                    jsonObject.put(i.replace("int_", ""), Integer.parseInt(returnContextTable(context, tableIndex).get(row).get(i)));
                } else {
                    jsonObject.put(i, returnContextTable(context, tableIndex).get(row).get(i));
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
        return jsonObject;
    }

    public JSONArray setJsonArray(String context, int tableIndex, int numberOfRow) throws JSONException {
        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < numberOfRow; i++) {
            JSONObject objects = setJsonObject(context, tableIndex, i);
            jsonArray.put(objects);
        }
        return jsonArray;
    }


}